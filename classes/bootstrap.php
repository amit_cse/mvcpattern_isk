<?php 
class Bootstraping
{
	private $controller;
	private $action;
	private $request;

	public function __construct($request)
	{
		$this->request = $request;
		if ($this->request['controller'] == "" ) {
			$this->controller = 'home';
		} else {
			$this->controller = $this->request['controller'];
		}
		if ($this->request['action'] == "") {
			$this->action = 'index';
		} else {
			$this->controller = $this->request['action']; 
		}
		
	}
	public function createController()
	{
		//check class
		if (class_exists($this->controller)) {
			$parent =class_parents($this->controller);
			// check Extend
			if (in_array("controller", $parents)) {
				if (method_exists($this->controller, action)) {
					return new $this->controller($this->action, $this->request);
				} else {
					//method does not exist
					echo "<h1>Method does not exist</h1>";
				}
			} else {
				//base controller not found
				echo "<h1>Base controller not found </h1>";
			}
		} else {
			//class does not exist
			echo "<h1>Controller class does not exist</h1>";
		}
	}
}
//$obj = new Bootstrap($_GET);
?>