<?php 
//start session
session_start();
// include config
require('config.php');
require('classes/bootstrap.php');
require('classes/bootstrap.php');
require('classes/Model.php');

require('controller/home.php');
require('controller/user.php');
require('controller/share.php');

require('models/home.php');
require('models/user.php');
require('models/share.php');

/*spl_autoload_register(function($class_name){
	include $class_name.'.php';*/

$bootstrap = new Bootstraping($_GET);
$controller = $bootstrap->createController();
if ($controller) {
	$controller->executeAction();
}
?>