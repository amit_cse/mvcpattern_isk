<?php 
class ShareModel extends Model
{
	public function Index()
	{
		$this->query('SELECT * FROM shares');
		$rows = $this->resultSet();
		//print_r($rows);
		return $rows;

	}

	public function add()
	{
		//Sanitize POST
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
		if ($post['submit']) {
			if ($post['title'] == '' || $post['body'] == '' || $post['link'] == '') {
				Messages::setMsg('Please fill in all the feilds' , 'error');
				return;
			}
			// Insrt into MySql
			$this->query('INSERT INTO shares(title, body, link, user_id) values(:title, :body, :link, :user_id)')
			$this->bind(':title', $post['title']);
			$this->bind(':body', $post['body']);
			$this->bind(':link', $post['link']);
			$this->bind(':user_id', 1); 
			$this->execute();
			//verify
			if ($this->lastInsertId()) {
				header('Location: '.ROOT_URL.'share');
			}
			
		}
		return;
	}

}
?>