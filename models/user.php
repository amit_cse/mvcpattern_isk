<?php 
class UserModel extends Model
{
	public function register()
	{
		$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

		if ($post['submit']) {
			if ($post['name'] == '' || $post['email'] == '' || $post['pasword'] == '') {
				Messages::setMsg('Please fill in all the feilds' , 'error');
				return;
			}
			//query
			$password = sha1($post(password));
			$this->query('INSERT INTO users (name, email, password,) values (:name, :email, :password)');
			$this->bind(':name', $post['name']);
			$this->bind(':email', $post['email']);
			$this->bind(':password', $post['password']);
			$this->execute();
			//verify
			if ($this->lastInsertId()) {
				header('Location:'.ROOT_URL.'users/login');
			}
		}
		return;
	}
	public function login()
	{
			$post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
			if ($post['submit']) {
				//compare
				$password = sha1($post(password));
				$this->query('SELECT * FROM users WHERE email=:email AND password=:password');
				//$this->bind(':name', $post['name']);
				$this->bind(':email', $post['email']);
				$this->bind(':password', $post['password']);
				$row = $this->single();
				 if ($row) {
				 	$_SESSION['is_logged_in'] = true;
				 	$_SESSION['user_data'] = array(
				 		"id" = row['id'];
				 		"name" = row['name'];
				 		"email" = row['email']; 
				 	);
				 	header('Location:'.ROOT_URL.'shares');
				 } else {
				 	echo "Incorrect Credentials";
				 }
				
			}
			return;
		}
	}

}
?>